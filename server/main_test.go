package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"

	"gitlab.com/upchieve/two-am-takeout/server/handlers"
)

var ctx context.Context
var cancel context.CancelFunc

type ServerSuite struct {
	suite.Suite
	ctx    context.Context
	cancel context.CancelFunc
}

//Frontend JSON message format: {"text":"hello","uuid":"2dc489b8-5009-40e1-ac99-07f2df90c4a8", "author": "Tony"}
type Message struct {
	Text string `json:"text"`
	Uuid string `json:"uuid"`
	Author string `json:"author"`
}

func (suite *ServerSuite) SetupSuite() {
	ctx = context.Background()
	suite.ctx, suite.cancel = context.WithCancel(ctx)

	go startServer(ctx)

	time.Sleep(1000)
}

func (suite *ServerSuite) TeardownSuite() {
	suite.cancel()
}

func (suite *ServerSuite) TestHealthCheck() {
	resp, err := http.Get("http://localhost:3000/health")
	assert.Nil(suite.T(), err)
	assert.Equal(suite.T(), http.StatusOK, resp.StatusCode)

	respData, err := ioutil.ReadAll(resp.Body)
	assert.Nil(suite.T(), err)

	var respBody handlers.HealthBody
	err = json.Unmarshal(respData, &respBody)
	assert.Nil(suite.T(), err)

	// make sure health body works matches what the frontend is expecting
	respString := string(respData)
	assert.Contains(suite.T(), respString, "\"ok\":true")
}

func (suite *ServerSuite) TestWebsockets() {
	u := url.URL{Scheme: "ws", Host: "localhost:3000", Path: "/ws"}

	reader, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	defer reader.Close()
	assert.Nil(suite.T(), err)

	sender, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	defer sender.Close()
	assert.Nil(suite.T(), err)

	// checking that text formatted messages go through and can be read correctly
	// The frontend sends the text & the UUID to the backend; 
	chatMsg := &Message{
		Text: "Hello, Websocket!",
		Uuid: "2dc489b8-5009-40e1-ac99-07f2df90c4a8",
		Author: "Tony",
	}
	json, _ := json.Marshal(chatMsg)
	stringJson := string(json)
	sender.WriteMessage(websocket.TextMessage, []byte(stringJson))

	_, message, err := reader.ReadMessage()
	assert.Nil(suite.T(), err)
	// log.Println(string(message)) //good-ole println to see what's in the message
	assert.Equal(suite.T(), stringJson, string(message))
}

func TestServerSuite(t *testing.T) {
	suite.Run(t, new(ServerSuite))
}

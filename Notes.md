### Thought process on fixing bug #1 (i.e. respBody not matching what was expected):

1. Ran the test to see what's wrong.
2. It's indicated the line #55 is throwing an error since the respBody of { OK: true } is not the same as { ok: true }.
3. Read through the test again to see where it's getting the respBody.
4. It's invoking a GET on URI '/health'.
5. Find out where the http server is instantiated; it's found the 'SetupSuite'
6. Investigated where the 'startServer' is located in the main.go file and its corresponding handler.
7. Investigated the handlers.go file and the 'ServerHealth' function.
8. Followed the hint on the Struct article on DigitalOcean. The section on JSON seemed most relevant and indicated it might have to do with the camel casing of the JSON prop. Tried lower casing the 'Ok' field and ran the test again.
9. An error about the export was raised, and so I lowercased the json tag as well. The test passed.

### Thought process on fixing bug #2 (i.e. backend test made the wrong assumptions on the frontend message format):

1. Let's run the server and check what the frontend is doing. Open two chat windows and see what happens.
2. So in going through the frontend code, we see that _line 103_ in _Chat.Vue_ is sending the chat message along with an UUID.
3. Let's check where is that handled in the backend.
4. As I do not know Golang and it's been a while since I last touched web sockets, I googled for a crash course on the Golang (looks a lot like the programming language C), working in conjunction on web sockets.
5. A lot of stepping through the code in _main.go_, _client.go_, and _handlers.go_ and logging through the data along the way. I could see the message json of 'text' & 'uuid' being passed from the frontend.
6. Created the the same _struct_ in the test to represent the _text_ & _uuid_ and made sure the message read is the same as the message sent.

### Thought process on feature #3 (asking the user for a name and sending it along with each message)

1. Let's look through the code to see how the code is structured.
2. Look through Vuetify documents for relevant components
3. I envisioned the name as a mandatory field, making the modal (v-dialog) a great option; it would appear when the page loads.
4. Added some simple validadtion rules.
5. Once the name is entered, it's passed as prop to the Chat component to be used in the chat bubbles (v-chip) and broadcasted to other clients.
